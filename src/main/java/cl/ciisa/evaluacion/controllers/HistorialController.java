package cl.ciisa.evaluacion.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import cl.ciisa.evaluacion.models.entitys.Diccionario;
import cl.ciisa.evaluacion.models.repository.IDiccionarioRepository;

@Controller
public class HistorialController {

	@Autowired
	IDiccionarioRepository diccionarioRepository;

	@RequestMapping("/historial")
	public String index(Model modelo) {
		List<Diccionario> historial=diccionarioRepository.findAll();
		modelo.addAttribute("historial", historial);
		return "historial";
	}

}
