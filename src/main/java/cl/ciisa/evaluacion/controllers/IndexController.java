package cl.ciisa.evaluacion.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;

import cl.ciisa.evaluacion.models.entitys.Diccionario;
import cl.ciisa.evaluacion.services.DiccionarioServiceImpl;

@Controller
public class IndexController {

	@Autowired
	DiccionarioServiceImpl diccionarioService;
	
	@RequestMapping("/")
	public String index() {
		return "index";
	}

	@GetMapping("/significado")
	public String resultado(@RequestParam (name="txtBuscar", required=false) String palabra, Model model){
		String significado=diccionarioService.getSignificado(palabra);
	    model.addAttribute("palabra",palabra);
		model.addAttribute("significado",significado);
		save(palabra);
	
		return "resultados";
	}
	
	@ResponseStatus(HttpStatus.CREATED)
	public void save(String palabra) {
		System.out.println("Registro de historial correcto!");
		diccionarioService.create(new Diccionario(palabra));
	}

}
