package cl.ciisa.evaluacion.services;

import java.nio.charset.Charset;

import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import cl.ciisa.evaluacion.models.entitys.Diccionario;
import cl.ciisa.evaluacion.models.repository.IDiccionarioRepository;


@Service
public class DiccionarioServiceImpl {

	private RestTemplate restTemplate;
	String appId = "3337fb78";
	String appKey = "7ec1b2b1ca8fd9efabcf8c48f17182d6";
	
	@Autowired
	IDiccionarioRepository diccionarioRepositoryLocal;

	@Autowired
	public DiccionarioServiceImpl(RestTemplateBuilder builder) {
		this.restTemplate = builder.build();
	}

	public String getSignificado(String palabra) {
		return restTemplate.exchange("https://od-api.oxforddictionaries.com/api/v2/entries/es/" + palabra,
				HttpMethod.GET, new HttpEntity<Diccionario>(createHeaders(appId, appKey)), String.class).getBody();
	}
	
	
	public Diccionario create(Diccionario entity) {
		return diccionarioRepositoryLocal.save(entity);
	}

	HttpHeaders createHeaders(String username, String password) {
		return new HttpHeaders() {
			{
				String auth = username + ":" + password;
				byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(Charset.forName("US-ASCII")));
				//String authHeader = "Basic " + new String(encodedAuth);
				//set("Authorization", authHeader);
				set("app_key", appKey);
				set("app_id", appId);
			}
		};
	}

}
