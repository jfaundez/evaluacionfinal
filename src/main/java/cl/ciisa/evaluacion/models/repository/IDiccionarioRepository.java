package cl.ciisa.evaluacion.models.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import cl.ciisa.evaluacion.models.entitys.Diccionario;

public interface IDiccionarioRepository extends JpaRepository<Diccionario,Integer> {

	
	
}
