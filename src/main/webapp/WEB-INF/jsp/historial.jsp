    <%@ page language="java" contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1" isELIgnored="false"%> 
  
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Historial</title>
</head>
<body>

<h1>Historial de busquedas</h1>

<c:forEach items="${historial}" var="item">

   ${item.id} : ${item.busqueda} : ${item.fecha} <br/>

</c:forEach>


</body>
</html>