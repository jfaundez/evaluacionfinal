<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Buscador de palabras</title>
</head>


<style>
body {
	font-family: sans-serif;
}

input[type="text"], [type="submit"] {
	padding: 10px;
	border: 1px solid #e0e0e0;
	border-radius: 10px;
}

input[type="submit"] {
	background:#00b3ff;
	color:white;}

body {
	display: flex;
	justify-content: center;
	align-items: center;
	min-height: 100vh;
	background: #f7f7f7;
}

.container {
	width: 70%;
	display: flex;
	flex-direction: column;
	justify-content: center;
	align-items: center;
	justify-content: center;
}

form {
	display: flex;
	width: 350px;
	min-height: 100px;
	flex-direction: column;
	align-items: center;
	justify-content: space-between;
	padding: 16px;
	background: white;
	box-shadow: 5px 5px 15px -4px #00000036;
	border-radius: 10px 10px 10px 10px;
}
</style>
<body>

	<div class="container">

		<h1>API REST Oxford Dictionaries</h1>

		<form action="/significado" method="get">
			<label>Ingresar palabra</label> <input name="txtBuscar" type="text"
				placeholder="Buscar..." required></input> <input type="submit"
				value="B�squeda"></input>

		</form>

		<a href="/historial">Ver historial</a>

		<footer>
			<p>Jean Carlos Faundez Saez</p>
			<p>Ingenieria en Inform�tica - CIISA</p>
			<p>Taller de aplicaciones empresariales</p>
			C�digo alojado en repositorio Bitbucket <a
				href="https://jfaundez@bitbucket.org/jfaundez/evaluacionfinal.git">Click
				aqu�</a>
		</footer>


	</div>















</body>
</html>